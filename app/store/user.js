
import ApiAction from '~/utils/apiAction.util';

const api = new ApiAction('users');

export const state = () => ({
  users: [],
});

export const mutations = {
  setUsers(state, users) {
    state.users = users
  },
}

export const getters = {
  getUsers:state => {
    return state.users
  }
}

export const actions = {

  async find({ commit },config = {}){

    let page = config.page;
    let numberPage = config.numberPage || 10;
    console.log(page,numberPage,config);
    return new Promise((resolve, reject) => {

      const query = '';
      return api.find(query).then((data)=>{
        data.forEach(element => {
          element.state = 0;
        });
        return resolve(data.slice((page*numberPage),(page*numberPage)+numberPage));
      }).catch((error)=>{
        return reject(error);
      });
    });
  },
}
