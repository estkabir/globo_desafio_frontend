import Package from './../../package.json';
const Enviroments = {}

//NODE ENVIROMENT
if(process.env.NODE_ENV === 'development'){
    Enviroments.API_URL = 'http://localhost:5001';
}else{
    Enviroments.API_URL = 'http://5dfff3be1fb9950014140318.mockapi.io'
}

//VERSION
Enviroments.VERSION = `${process.env.NODE_ENV}/${Package.version}`;


export default Enviroments;