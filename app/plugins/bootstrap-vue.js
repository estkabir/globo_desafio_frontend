
import Vue from 'vue';

import {
  LayoutPlugin,
  NavbarPlugin,
  FormPlugin,
  ButtonPlugin,
  FormInputPlugin,
  ModalPlugin,
  CardPlugin,
  FormGroupPlugin,
  FormRadioPlugin,
  FormTextareaPlugin,
  FormCheckboxPlugin,
  ListGroupPlugin,
  PaginationPlugin,
  TablePlugin,
  SpinnerPlugin
} from 'bootstrap-vue';

Vue.use(LayoutPlugin);
Vue.use(NavbarPlugin);
Vue.use(FormPlugin);
Vue.use(ButtonPlugin);
Vue.use(FormInputPlugin);
Vue.use(ModalPlugin);
Vue.use(CardPlugin);
Vue.use(FormGroupPlugin);
Vue.use(FormRadioPlugin);
Vue.use(FormTextareaPlugin);
Vue.use(FormCheckboxPlugin);
Vue.use(ListGroupPlugin);
Vue.use(PaginationPlugin);
Vue.use(TablePlugin);
Vue.use(SpinnerPlugin);