import Vue from 'vue';
import dayjs from 'dayjs';

import 'dayjs/locale/es' // load on demand
import 'dayjs/locale/pt' // load on demand

Vue.prototype.$day = dayjs;