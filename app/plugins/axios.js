import axios from 'axios';
import Enviroments from '~/config/enviroments';

export default () => {

  const baseURL = Enviroments.API_URL;
  let headers = {};

  // const jwt = AuthUtil.getJwtFromLocalStorage();
  // if(jwt){
  //   headers['Authorization'] =  `bearer ${jwt}`
  // };
  return axios.create({baseURL,headers });
}