
import axios from '~/plugins/axios';

export default class ApiAction {

  constructor(path) {
    this.endPoint = path;
  }

  // Getter
  get endPoint() {
    return this._endPoint;
  }

  // Setters
  set endPoint(path){
    this._endPoint = path;
  }

  // Method
  async findOne(id){

    return new Promise((resolve, reject) => {

      return axios()({url: `/${this.endPoint}/${id}`,method: 'GET'}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async find(query){

    return new Promise((resolve, reject) => {

      return axios()({url: `/${this.endPoint}?${query}`,method: 'GET'}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async count(query){

    return new Promise((resolve, reject) => {

      return axios()({url: `/${this.endPoint}/count?${query}`,method: 'GET'}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async create(data){

    return new Promise((resolve, reject) => {

      return axios()({url: '/'+this.endPoint,method: 'POST',data}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async edit(id,data){

    return new Promise((resolve, reject) => {

      return axios()({url: `/${this.endPoint}/${id}`,method: 'PUT',data}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async remove(id){

    return new Promise((resolve, reject) => {

      return axios()({url: `/${this.endPoint}/${id}`,method: 'DELETE'}).then((response)=>{

        return resolve(response.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }

  async graphqlQuery(query){

    return new Promise((resolve, reject) => {

      return axios()({url: `/graphql`,method: 'POST',data:{query}}).then((response)=>{


        const request = response.request;
        const rawResponse = JSON.parse(request.response);
        const keys = Object.keys(rawResponse);
        keys.forEach(key => {
          if(key === 'errors') return reject({status:404});
        });
        return resolve(response.data.data);
      }).catch((error)=>{
        return reject(error.response);
      });
    });
  }
}