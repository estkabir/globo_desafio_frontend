

module.exports = {

  srcDir: 'app/',
  server: {
    port: 5000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },
  config: {
    productionTip: false,
    devtools: true
  },

  //buildDir: 'nuxt-dist',
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Audiowide&display=swap' }
    ]
  },
  /*
  ** Expose env variables
  */
  env: {},
  /*
  ** Customize the progress bar color
  */
  //loading: '~/components/commons/loading.vue',
  loading: {
    color:'black', //	CSS color of the progress bar [type: String]
    failedColor:	'red', //	CSS color of the progress bar when an error appended while rendering the route (if data or fetch sent back an error for example).[type: String]
    height:	'5px',	//Height of the progress bar (used in the style property of the progress bar)[type: String]
    throttle:	200	,//In ms, wait for the specified time before displaying the progress bar. Useful for preventing the bar from flashing.[type: Number]
    duration:	5000 ,//	In ms, the maximum duration of the progress bar, Nuxt.js assumes that the route will be rendered before 5 seconds.[type: Number]
    continuous:	false, //	Keep animating progress bar when loading takes longer than duration.[type: Boolean]
    css	:true,	//Set to false to remove default progress bar styles (and add your own).[type: Boolean]
    rtl:	false,	//Set the direction of the progress bar from right to left.[type: Boolean]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.devtool = isClient ? 'source-map' : 'inline-source-map'
      }
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faCircle','faDesktop','faHistory','faSearch',
          'faUser','faSlidersH','faHome','faCog','faPowerOff','faEllipsisH',
          'faTimes','faCalendarAlt','faCaretDown','faTrash','faPen','faDownload'
          ]
        }
      ]
    }]
  ],
  plugins: [
    '~/plugins/bootstrap-vue.js',
    '~/plugins/vue-day.js',
    '~/plugins/vue-logger.js'
  ],

  filenames:{
    app: ({ isDev }) => isDev ? '[name].js' : '[chunkhash].js',
    chunk: ({ isDev }) => isDev ? '[name].js' : '[chunkhash].js',
    css: ({ isDev }) => isDev ? '[name].css' : '[contenthash].css',
    img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'img/[hash:7].[ext]',
    font: ({ isDev }) => isDev ? '[path][name].[ext]' : 'fonts/[hash:7].[ext]',
    video: ({ isDev }) => isDev ? '[path][name].[ext]' : 'videos/[hash:7].[ext]'
  },
  indicator: true
}

