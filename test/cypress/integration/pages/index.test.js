describe('Home page', function() {

  beforeEach(() => {
    cy.visit('/');
  });

  it('Check click to open sidebar', () => {

    cy.get('#filterModal').should('not.exist');
    cy.get('[data-test="header:open:sidebar"]').click()
    cy.get('#filterModal').should('exist');
  })
})