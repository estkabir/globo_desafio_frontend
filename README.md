# globo desafio frontend

Projeto feito com o framework [NuxtJs](https://nuxtjs.org/)

## Requerimentos

* **Node.js** >= 10.x
* **NPM** >= 6.x

## Instalação

```sh
npm install
```

## Modo Denvolvimento

Executa: **Nuxt** em modo desenvolvimento e [Json Server](https://github.com/typicode/json-server) para simular uma API.

```sh
npm start
```

## Build de produção (Single page application)

Arquivos são gerados na pasta *dist*

```sh
npm run build
```

## Demo online

Versão online de demostração neste [link](http://infotevs.labs2.s3-website-us-east-1.amazonaws.com/)

## Test unitário

Utilizado [Jest](https://jestjs.io/) e [@vue/test-utils](https://vue-test-utils.vuejs.org/)

```sh
npm run test
```

## Test integração

Utilizado o framework [Cypress](https://www.cypress.io/)

```sh
npm run test:cy open
```

obs: ```npm start``` deve estar sendo executado.
